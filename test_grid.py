import pytest
from grid import Grid


class TestGrid:

    def test_live_cell_2_live_neighbours(self):
        cells_alive = [(1, 1), (2, 2), (2, 1)]
        grid = Grid(cells_alive)
        assert(grid.cell_stays_alive((2, 2)) is True)

    def test_live_cell_3_live_neighbours(self):
        cells_alive = [(1, 1), (2, 2), (2, 1), (3, 2)]
        grid = Grid(cells_alive)
        assert(grid.cell_stays_alive((2, 2)) is True)

    def test_live_cell_4_live_neighbours(self):
        cells_alive = [(1, 1), (2, 2), (2, 1), (3, 2), (3, 3)]
        grid = Grid(cells_alive)
        assert(grid.cell_stays_alive((2, 2)) is False)

    def test_live_cell_less_than_2_live_neighbours(self):
        cells_alive = [(1, 1), (2, 2)]
        grid = Grid(cells_alive)
        assert(grid.cell_stays_alive((2, 2)) is False)

    # dead cell (2, 2) stays dead
    def test_dead_cell_2_live_neighbours(self):
        cells_alive = [(1, 1), (1, 2)]
        grid = Grid(cells_alive)
        assert(grid.cell_becomes_alive((2, 2)) is False)

    # dead cell (2, 2) becomes alive because three live neighbours
    def test_dead_cell_3_live_neighbours(self):
        cells_alive = [(1, 1), (1, 2), (1, 3)]
        grid = Grid(cells_alive)
        assert(grid.cell_becomes_alive((2, 2)) is True)

    # test multiple cells for next cell state
    def test_next_generation_of_alive_cells(self):
        cells_alive = [(1, 1), (1, 2), (1, 3)]
        initial_state = [(1, 1), (1, 2), (1, 3),
                         (2, 1), (2, 2), (2, 3),
                         (3, 1), (3, 2), (3, 3)]
        grid = Grid(cells_alive, initial_state)
        changed_state = set([(1, 2), (2, 2)])
        assert (grid.next_iteration() == changed_state)

    # test getting the matrix row length
    def test_get_row_length(self):
        initial_state = [(1, 1), (1, 2), (1, 3),
                         (2, 1), (2, 2), (2, 3),
                         (3, 1), (3, 2), (3, 3)]
        grid = Grid([], initial_state)
        assert(grid.get_row_length() == 3)

    def test_print_grid_state(self):
        cells_alive = [(1, 1), (1, 2), (1, 3)]
        initial_state = [(1, 1), (1, 2), (1, 3),
                         (2, 1), (2, 2), (2, 3),
                         (3, 1), (3, 2), (3, 3)]
        grid = Grid(cells_alive, initial_state)
        state = ("111\n000\n000")
        # set([(1, 2), (2, 2)])
        assert (grid.print_grid() == state)

