import math


class Grid:

    def __init__(self, cells, initial_state=None):
        self.live_cells = set(cells)
        self.initial_state = initial_state

    def get_valid_neightbours(self, cell):
        x = cell[0]
        y = cell[1]
        # do these all exist or no?
        return set([(x, y + 1),
                    (x, y - 1),
                    (x + 1, y),
                    (x + 1, y - 1),
                    (x + 1, y + 1),
                    (x - 1, y - 1),
                    (x - 1, y + 1),
                    (x - 1, y)])

    def get_alive_neighbours(self, cell):
        neighbours = self.get_valid_neightbours(cell)
        return self.live_cells.intersection(neighbours)

    def cell_stays_alive(self, cell):
        intersection = self.get_alive_neighbours(cell)
        return len(intersection) == 2 or len(intersection) == 3

    def cell_becomes_alive(self, cell):
        intersection = self.get_alive_neighbours(cell)
        return len(intersection) == 3

    def next_iteration(self):
        next_iteration = set([])
        for cell in self.initial_state:
            # is cell alive
            if cell in self.live_cells and self.cell_stays_alive(cell):
                next_iteration.add(cell)
            if cell not in self.live_cells and self.cell_becomes_alive(cell):
                next_iteration.add(cell)
        return next_iteration

    def get_row_length(self):
        return int(math.sqrt(len(self.initial_state)))

    def print_grid(self):
        row_length = self.get_row_length()
        # pring zeros except for self.live_cells
        grid = ""
        next_row_len = 0
        for cell in self.initial_state:
            if row_length == next_row_len:
                 next_row_len = 0
                 grid = grid + "\n"
            if cell in self.live_cells:
                grid = grid +  "1"
            else:
                grid = grid + "0"
            next_row_len = next_row_len + 1
        return grid
